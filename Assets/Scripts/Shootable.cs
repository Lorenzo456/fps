﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shootable : MonoBehaviour {

    public int health;

	// Use this for initialization
	void Start () {
		
	}

    public virtual void OnHit()
    {
        health--;
    }
}
