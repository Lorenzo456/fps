﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoldierFieldOfViewScript : MonoBehaviour {

    public float fieldOfViewAngle = 20f;
    public bool playerInSight;
    public Vector3 lastPlayerSighting;
    Vector3 previousSighting;
    SphereCollider fieldOfViewCollider;
    Soldier soldier;
    public void Start()
    {
        fieldOfViewCollider = GetComponentInParent<SphereCollider>();
        soldier = GetComponentInChildren<Soldier>();
    }


    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Debug.Log("PLAYER IN FIELD");
            playerInSight = false;
            Vector3 direction = other.transform.position - transform.position;
            float angle = Vector3.Angle(direction, transform.forward);

            if (angle < fieldOfViewAngle * 0.5f)
            {
                RaycastHit hit;
                if (Physics.Raycast(transform.position + new Vector3(0, 1, 0), direction.normalized, out hit, fieldOfViewCollider.radius))
                {
                    if (other.CompareTag("Player"))
                    {
                        playerInSight = true;
                        soldier.playerInSight = true;
                        lastPlayerSighting = Player.instance.transform.position;
                    }
                }
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player") && playerInSight)
        {
            playerInSight = false;
        }
    }

    private void Update()
    {
        DebugFieldOfView();
    }

    public void DebugFieldOfView()
    {
        Vector3 noAngle = Vector3.Normalize(transform.forward);
        Quaternion spreadAngleStart = Quaternion.AngleAxis(-fieldOfViewAngle * 0.5f, new Vector3(0, 1, 0));
        Quaternion spreadAngleEnd = Quaternion.AngleAxis(fieldOfViewAngle * 0.5f, new Vector3(0, 1, 0));

        Vector3 newVector1 = spreadAngleStart * noAngle;
        Vector3 newVector2 = spreadAngleEnd * noAngle;

        Debug.DrawRay(transform.position + new Vector3(0, 1, 0), transform.forward * fieldOfViewCollider.radius, Color.black);
        Debug.DrawRay(transform.position + new Vector3(0, 1, 0), newVector1 * fieldOfViewCollider.radius, Color.black);
        Debug.DrawRay(transform.position + new Vector3(0, 1, 0), newVector2 * fieldOfViewCollider.radius, Color.black);
    }

}
