﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationEventHandler : MonoBehaviour {

    public Weapon weapon;
    public FPSController fpsController;

	// Use this for initialization
	void Start () {
        fpsController = GetComponentInParent<FPSController>();
	}

    public void Reload()
    {
        Player.instance.weapons[Player.instance.currentWeapon].GetComponent<Weapon>().Reload();
    }

    public void Reloaded()
    {
        Player.instance.weapons[Player.instance.currentWeapon].GetComponent<Weapon>().Reloaded();
    }

    public void Aim()
    {
        Player.instance.weapons[Player.instance.currentWeapon].GetComponent<Weapon>().Aim();
    }

    public void ReloadVibration()
    {
        Player.instance.weapons[Player.instance.currentWeapon].GetComponent<Weapon>().ReloadVibration();
    }

    public void WeaponSwapped()
    {
        fpsController.WeaponSwapped();
    }
}
