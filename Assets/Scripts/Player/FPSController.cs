﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSController : MonoBehaviour {

    public float speed = 2f;
    public float originalSpeed;
    public float aimSpeedMultiplyer = 1;
    public float crouchSpeedMultiplyer = 1;
    public float currentSpeed;
    public float sensitivity = 2f;
    public GameObject Eyes;
    public float gravity = 15F;
    public float verticalVelocity;

    float moveFB;
    float moveLR;
    float rotationX;
    float rotationY;
    public float jumpForce = 7;
    public bool toggleCrouch;
    
    CharacterController player;
    Animator animator;

	void Start () {
        player = GetComponent<CharacterController>();
        animator = GetComponentInChildren<Animator>();
        originalSpeed = speed;
	}

    private void Update()
    {
        if (Input.GetButtonDown("Shoot"))
        {
            Player.instance.pullingTrigger = true;
        }

        if (Input.GetButtonUp("Shoot"))
        {
            Player.instance.pullingTrigger = false;
        }

        if (Input.GetButtonDown("Reload"))
        {
            Player.instance.reloading = true;
        }

        if (Input.GetButtonUp("Reload"))
        {
            Player.instance.reloading = false;
        }

        if (Input.GetButtonDown("SwapWeapon"))
        {
            Player.instance.swapWeapon = true;
            SwapWeapon();
        }

        if (Input.GetButtonDown("Aim"))
        {
            Player.instance.aiming = true;
        }

        if (Input.GetButtonUp("Aim"))
        {
            Player.instance.aiming = false;
        }
        CheckCrouch();
        Movement();
    }


    void SwapWeapon()
    {
        if (Player.instance.weapons.Length > 0)
        {
            for (int i = 0; i < Player.instance.weapons.Length; i++)
            {
                Player.instance.weapons[i].SetActive(false);
            }

            if (Player.instance.currentWeapon >= Player.instance.weapons.Length -1)
            {
                Player.instance.currentWeapon = 0;
            }
            else
            {
                Player.instance.currentWeapon++;
            }
            animator.SetTrigger("SwapWeapon");
        }
    }

    public void WeaponSwapped()
    {
        Player.instance.weapons[Player.instance.currentWeapon].SetActive(true);
        Player.instance.swapWeapon = false;
    }

    void CheckCrouch()
    {
        
        if (Input.GetButtonDown("Crouch"))
        {
            if (toggleCrouch)
            {
                if (!Player.instance.crouching)
                {
                    Player.instance.crouching = true;
                    animator.SetBool("Crouch", true);
                }
                else
                {
                    Player.instance.crouching = false;
                    animator.SetBool("Crouch", false);
                }
            }
            else
            { 
                Player.instance.crouching = true;
                animator.SetBool("Crouch", true);
            }

        }

        if (Input.GetButtonUp("Crouch"))
        {
            if (!toggleCrouch)
            {
                Player.instance.crouching = false;
                animator.SetBool("Crouch", false);
            }
        }

        if (Player.instance.crouching)
        {
            crouchSpeedMultiplyer = 0.5f;
        }
        else
        {
            crouchSpeedMultiplyer = 1;
        }

    }

    void CameraMovement()
    {
        rotationX = Input.GetAxis("Mouse X") * sensitivity;
        transform.Rotate(0, rotationX, 0) ;
        
        rotationY += Input.GetAxis("Mouse Y") * sensitivity;

        rotationY = Mathf.Clamp(rotationY, -40, 40);
        Eyes.transform.eulerAngles = new Vector3(-rotationY, Eyes.transform.eulerAngles.y, 0);
        
    }

    void Movement() {

        moveFB = Input.GetAxis("Vertical") * speed;
        moveLR = Input.GetAxis("Horizontal") * speed;
        


        Vector3 movement = new Vector3(moveLR, verticalVelocity, moveFB);
        movement = Vector3.Normalize(transform.rotation * movement);

        if (player.isGrounded)
        {
            verticalVelocity = -gravity * Time.deltaTime;
            if (Input.GetButtonDown("Jump"))
            {
                //    Debug.Log("JUMPP");
                verticalVelocity = jumpForce;
                movement.y = verticalVelocity * Time.deltaTime;
            }
        }
        else
        {
            verticalVelocity -= gravity * Time.deltaTime;

        }

        currentSpeed = speed * aimSpeedMultiplyer * crouchSpeedMultiplyer;


        player.Move(movement * currentSpeed * Time.deltaTime);

        // Eyes.transform.Rotate(-rotationY,0, 0);
        CameraMovement();

    }
}
