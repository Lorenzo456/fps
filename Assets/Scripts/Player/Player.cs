﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour, IDamageable {

    public GameObject[] weapons;
    public GameObject weaponHolder;
    public int currentWeapon = 0;
    public static Player instance = null;
    public Image reticle;
    public Image crosshairUp;
    public Image crosshairDown;
    public Image crosshairLeft;
    public Image crosshairRight;
    public GameObject crosshair;
    public Transform crosshairOriginalTransform;

    public GameObject feedBackReticle;
    bool feedBackhit;

    public bool aiming;
    public bool crouching;
    public bool shooting;
    public bool pullingTrigger;
    public bool reloading;
    public FPSController controller;
    public bool swapWeapon;
    public int Health;
    public int currentHealth;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);

    }

    // Use this for initialization
    void Start () {
        reticle = GameObject.Find("Reticle").GetComponent<Image>();

        crosshair = GameObject.Find("Crosshair");
        crosshairOriginalTransform = crosshair.transform;

        crosshairUp = GameObject.Find("CrosshairUp").GetComponent<Image>();
        crosshairDown = GameObject.Find("CrosshairDown").GetComponent<Image>();
        crosshairLeft = GameObject.Find("CrosshairLeft").GetComponent<Image>();
        crosshairRight = GameObject.Find("CrosshairRight").GetComponent<Image>();
        feedBackReticle = GameObject.Find("FeedbackHitHolder");
        feedBackReticle.SetActive(false);

        controller = GetComponent<FPSController>();

        weaponHolder = GameObject.Find("WeaponHolder0");
        for(int i = 0; i < weapons.Length ; i++)
        {

            weapons[i] = weaponHolder.transform.GetChild(i).gameObject;
            weapons[i].SetActive(false);
            
        }
         weapons[currentWeapon].SetActive(true);
        currentHealth = Health;
    }

    // Update is called once per frame
    void Update () {
		
	}

    public void OnFeedBackHit(int health = 1)
    {
        feedBackhit = true;
        feedBackReticle.SetActive(true);
        if(health <= 0)
        {
            feedBackReticle.GetComponentInChildren<Image>().color = Color.red;
        }
        else
        {
            feedBackReticle.GetComponentInChildren<Image>().color = Color.white;

        }
        // Debug.Log("GOTHIT");
        StartCoroutine("OnFeedBackCheck");
    }
     IEnumerator OnFeedBackCheck()
    {
        yield return new WaitForSeconds(.1f);
        feedBackhit = false;
//        feedBackReticle.SetActive(true);
        yield return new WaitForSeconds(.1f);

        if (feedBackhit)
        {
         //   Debug.Log("RETURN GOT HIT AGAIN");
            yield return null;
        }
        else
        {
       //     Debug.Log("DISABLE FEEDBACK");
            feedBackReticle.SetActive(false);
        }
    }

    public void OnHit(int damageTaken = 0)
    {
        currentHealth -= damageTaken;
    }

    public int CheckHealth()
    {
        return currentHealth;
    }
}
