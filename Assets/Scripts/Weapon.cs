﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using XInputDotNetPure;

public class Weapon : MonoBehaviour {

    public Transform bullethole;
    public Vector3 originalWeaponPosition  = new Vector3(0,0, 0.271f);

    [Header("Bullet Settings")]
    public float fireRate = .25f;
    public float range = 50;
    public Transform muzzle;
    public int maxBullets;
    public int bullets;
    public int maxClip;
    public int currentClip;
    public bool reloading;
    public bool triggerRelease;
    public bool released;
    public bool shooting;
    public int damage = 1;

    [Header("Controller Settings")]
    protected GamePadState state;
    public float vibrationMotorLeftShoot;
    public float vibrationMotorRightShoot;
    public float vibrationReload;

    public Text bulletsUI;
    public Text clipUI;
    public Text WeaponNameUI;

    public Camera fpsCamera;
    public LineRenderer lineRenderer;
    public WaitForSeconds shotLenght = new WaitForSeconds(.02f);
    public AudioSource audioSource;
    public float nextFireTime;
    public Animator animator;
    public Light muzzleFlash;

    [Header("recoil Settings")]

    public float minRecoilx;
    public float maxRecoilx;

    public float minRecoily;
    public float maxRecoily;

    protected float tempRecoilMinX;
    protected float tempRecoilMinY;
    protected float tempRecoilMaxX;
    protected float tempRecoilMaxY;
    protected Vector3 recoilVector;

    // Use this for initialization
    void Start () {
        lineRenderer = GetComponent<LineRenderer>();
        audioSource = GetComponent<AudioSource>();
        fpsCamera = GetComponentInParent<Camera>();
        muzzle = GameObject.Find("Muzzle").transform;
        animator = GetComponentInParent<Animator>();
        muzzleFlash = GetComponentInChildren<Light>();

        bulletsUI = GameObject.Find("BulletsUI").GetComponent<Text>();
        clipUI = GameObject.Find("ClipUI").GetComponent<Text>();
        WeaponNameUI = GameObject.Find("WeaponNameUI").GetComponent<Text>();

        bulletsUI.text = bullets.ToString();
        clipUI.text = currentClip.ToString();
        WeaponNameUI.text = transform.name;

        muzzleFlash.enabled = false;
        bullethole = GameObject.Find("BulletHoleMaker").GetComponent<Transform>();
        transform.localPosition = originalWeaponPosition;
    }

    // Update is called once per frame
    public virtual void Update () {
        if (Player.instance.swapWeapon)
            return;

        if (bulletsUI && clipUI)
        {
            bulletsUI.text = bullets.ToString();
            clipUI.text = currentClip.ToString();
            WeaponNameUI.text = transform.name;
        }

        if (!Player.instance.pullingTrigger && !shooting)
        {
            released = true;
        }

        RaycastHit hit;
        Vector3 rayOrigin = fpsCamera.ViewportToWorldPoint(new Vector3(.5f, .5f, 0));
        if (Physics.Raycast(rayOrigin, fpsCamera.transform.forward, out hit, range))
        {

            OnRayCastHover(hit);

            //OnShoot
            if (Player.instance.pullingTrigger && Time.time > nextFireTime && !reloading)
            {
                if (triggerRelease && !released)
                    return;

                if (currentClip <= 0)
                    return;

                Shoot(hit.point);
                if (hit.transform.CompareTag("Destroyable"))
                {
                    hit.transform.GetComponent<IDamageable>().OnHit(damage);
                    Player.instance.OnFeedBackHit(hit.transform.GetComponent<IDamageable>().CheckHealth());
                }
            }
        }
        else
        {
            Player.instance.reticle.enabled = true;
            Player.instance.crosshairUp.color = Color.white;
            Player.instance.crosshairDown.color = Color.white;
            Player.instance.crosshairLeft.color = Color.white;
            Player.instance.crosshairRight.color = Color.white;

            if (Player.instance.pullingTrigger && Time.time > nextFireTime && !reloading)
            {
                if (triggerRelease && !released)
                    return;

                if (currentClip <= 0)
                    return;

                Shoot();
            }

        }

        if (Player.instance.aiming)
        {
            Player.instance.controller.aimSpeedMultiplyer = .5f;
            Player.instance.crosshair.SetActive(false);
            animator.SetBool("Aiming", true);
            //Debug.Log("AIMING");
        }

        if (!Player.instance.aiming)
        {
            Player.instance.controller.aimSpeedMultiplyer = 1;
            Player.instance.crosshair.SetActive(true);
            animator.SetBool("Aiming", false);
            Player.instance.aiming = false;
        }

        if (Player.instance.reloading)
        {
            Reload();
        }
    }

    public virtual void OnRayCastHover(RaycastHit hit)
    {
        //ON Hover
        if (hit.transform.CompareTag("Destroyable"))
        {
            //   Debug.Log("INTERACTABLE");
            Player.instance.reticle.enabled = false;

            Player.instance.crosshairUp.color = Color.red;
            Player.instance.crosshairDown.color = Color.red;
            Player.instance.crosshairLeft.color = Color.red;
            Player.instance.crosshairRight.color = Color.red;

        }
        else
        {
            Player.instance.reticle.enabled = true;
            Player.instance.crosshairUp.color = Color.white;
            Player.instance.crosshairDown.color = Color.white;
            Player.instance.crosshairLeft.color = Color.white;
            Player.instance.crosshairRight.color = Color.white;
        }
    }

    public virtual void Shoot(Vector3 Point = default(Vector3))
    {
        released = false;
        animator.SetTrigger("Shooting");
        currentClip--;
        clipUI.text = currentClip.ToString();
        audioSource.Play();
        nextFireTime = Time.time + fireRate;
        lineRenderer.SetPosition(0, transform.position);


        if (Player.instance.aiming)
        {
            tempRecoilMaxX = 0;
            tempRecoilMaxY = 0;
            tempRecoilMinX = 0;
            tempRecoilMinY = 0;
        }
        else
        {
            tempRecoilMaxX = maxRecoilx;
            tempRecoilMaxY = maxRecoily;
            tempRecoilMinX = minRecoilx;
            tempRecoilMinY = minRecoily;
        }

        if (Point == default(Vector3))
        {
            lineRenderer.SetPosition(1, fpsCamera.ViewportToWorldPoint(new Vector3(.5f, .5f, range / 2)) + new Vector3(Random.Range(tempRecoilMinX, tempRecoilMaxX), Random.Range(tempRecoilMinY, tempRecoilMaxY)));

            // GameObject bulletClone = Instantiate(Resources.Load("Prefabs/Bullet", typeof(GameObject)), muzzle.position + (muzzle.forward * .2f), Quaternion.identity) as GameObject;
            //bulletClone.transform.forward = (fpsCamera.transform.forward * range) + new Vector3(Random.Range(tempRecoilMinX, tempRecoilMaxX), Random.Range(tempRecoilMinY, tempRecoilMaxY), 0);
            //Debug.Log("NOHIT");
        }
        else
        {
            lineRenderer.SetPosition(1, Point + new Vector3(Random.Range(tempRecoilMinX, tempRecoilMaxX), Random.Range(tempRecoilMinY, tempRecoilMaxY), 0));
            // GameObject bulletClone = Instantiate(Resources.Load("Prefabs/Bullet", typeof(GameObject)), muzzle.position + (muzzle.forward * .2f), Quaternion.identity) as GameObject;
            // bulletClone.transform.LookAt(Point + new Vector3(Random.Range(tempRecoilMinX, tempRecoilMaxX), Random.Range(tempRecoilMinY, tempRecoilMaxY),0));
            //bulletClone.transform.forward = Point;
            //Debug.Log("HIT");
        }

        /*
        GameObject bulletClone = Instantiate(Resources.Load("Prefabs/Bullet", typeof(GameObject)), muzzle.position + new Vector3(0,0,.5f), muzzle.rotation) as GameObject;
        bulletClone.transform.forward = muzzle.forward;
        */

        // Debug.Log("SHOOT");
        StartCoroutine("ShotEffect");
        StartCoroutine(Vibration(vibrationMotorLeftShoot, vibrationMotorRightShoot, .1f));
    }

    public virtual void Reload()
    {
        if (!reloading && currentClip < maxClip && bullets > 0)
        {
            reloading = true;
            animator.SetTrigger("Reload");
        }
    }

    public virtual void Reloaded()
    {
        nextFireTime = Time.time + (fireRate *2);

        reloading = false;

        if (bullets > maxClip - currentClip)
        {
            //    Debug.Log("ENOUGH BULLETS");
            bullets = bullets - (maxClip - currentClip);
            currentClip = maxClip;
        }
        else
        {
            //  Debug.Log("NOT ENOUGH BULLETS"); 
            currentClip = currentClip + bullets;
            bullets = 0;
        }

    }

    public virtual void ReloadVibration()
    {

        StartCoroutine(Vibration(vibrationReload, vibrationReload, .05f));
    }

    public virtual IEnumerator ShotEffect()
    {
        bullethole.position = lineRenderer.GetPosition(1);
        //lineRenderer.enabled = true;
        muzzleFlash.enabled = true;
        yield return shotLenght;
        // lineRenderer.enabled = false;
        muzzleFlash.enabled = false;
        bullethole.position = new Vector3(0, -10, 0);
        released = false;

    }

    public virtual IEnumerator Vibration(float vibrationLeftMotor = 0, float vibrationRightMotor = 0, float vibrationTime = .1f)
    {
        state = GamePad.GetState(0);
        if (state.IsConnected)
        {
            GamePad.SetVibration(0, vibrationLeftMotor, vibrationRightMotor);
            yield return new WaitForSeconds(vibrationTime);
            GamePad.SetVibration(0, 0, 0);
        }
    }

    public virtual void Aim()
    {
        Player.instance.aiming = true;
    }
}
