﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Obsticlecube : MonoBehaviour, IKillable, IDamageable{

    public int maxHealth;
    public int currentHealth;

    // Use this for initialization
    void Start ()
    {
        currentHealth = maxHealth;
	}
	
	// Update is called once per frame
	void Update () {
        
        if (currentHealth <= 0)
        {
            Kill();
        }
        
	}

    public void Kill()
    {
        Destroy(gameObject);
    }

    //The required method of the IDamageable interface
    public void OnHit(int damageTaken)
    {
        //Do something fun
        currentHealth -= damageTaken;
    }

    public int CheckHealth()
    {
        return currentHealth;
    }
}
