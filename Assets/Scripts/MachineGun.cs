﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MachineGun : Weapon {


    public override void Update()
    {

    }

    public override void Shoot(Vector3 Point = default(Vector3))
    {

        currentClip--;
        audioSource.Play();
        nextFireTime = Time.time + fireRate;
        lineRenderer.SetPosition(0, muzzle.transform.position);

        tempRecoilMaxX = maxRecoilx;
        tempRecoilMaxY = maxRecoily;
        tempRecoilMinX = minRecoilx;
        tempRecoilMinY = minRecoily;

        RaycastHit hit;
        if (Physics.Raycast(muzzle.transform.position + new Vector3(Random.Range(tempRecoilMinX, tempRecoilMaxX), Random.Range(tempRecoilMinY, tempRecoilMaxY)), muzzle.forward, out hit, range))
        {
            if (hit.transform.CompareTag("Player"))
            {
                hit.transform.GetComponent<IDamageable>().OnHit(damage);
            }
        }

        if (Point == default(Vector3))
        {
            lineRenderer.SetPosition(1, (muzzle.forward * range) + new Vector3(Random.Range(tempRecoilMinX, tempRecoilMaxX), Random.Range(tempRecoilMinY, tempRecoilMaxY)));
        }

        // Debug.Log("SHOOT");
        StartCoroutine("ShotEffect");
    }

    public override void Reload()
    {
        reloading = true;
        StartCoroutine("ReloadTime");
    }

    public IEnumerator ReloadTime()
    {
        yield return new WaitForSeconds(5f);
        currentClip = 20;
        reloading = false;
    }

    public override IEnumerator ShotEffect()
    {
        bullethole.position = lineRenderer.GetPosition(1);
        //lineRenderer.enabled = true;
        muzzleFlash.enabled = true;
        yield return shotLenght;
        //lineRenderer.enabled = false;
        muzzleFlash.enabled = false;
        bullethole.position = new Vector3(0, -10, 0);
        released = false;

    }

}
