﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

[RequireComponent(typeof(Rigidbody))]
public class BaseEnemy : MonoBehaviour
{
    public GameObject player;
    protected FSMSystem fsm;
    public NavMeshAgent agent;

    protected int currentHp;
    public int maxHp = 10;
    public float speed = 5;
    public bool canShoot;

    public Weapon weapon;

    [HideInInspector] public Animator anim;

    public void SetTransition(Transition t) { fsm.PerformTransition(t); }

    public virtual void Start()
    {
        agent = GetComponentInParent<NavMeshAgent>();
        player = GameObject.Find("Player");
        anim = GetComponent<Animator>();
        currentHp = maxHp;
        agent.speed = speed;
        MakeFSM();
    }

    public virtual void FixedUpdate()
    {
        fsm.CurrentState.Reason(player, gameObject);
        fsm.CurrentState.Act(player, gameObject);
    }


    public virtual void MakeFSM()
    {

    }

    public virtual void OnDeath()
    {
        Destroy(gameObject);
    }

}
