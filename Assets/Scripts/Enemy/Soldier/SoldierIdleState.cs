﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoldierIdleState : FSMState {

    Soldier soldier;
    bool firstFrame;
    int currentPatrolPoint = -1;
    bool facingWP;

    bool waiting;
    float waitTime;
    float nextWaitTime;


    public SoldierIdleState()
    {
        stateID = StateID.SoldierIdleState;
    }

    public override void Act(GameObject player, GameObject npc)
    {
        if (!firstFrame || soldier == null)
        {
            firstFrame = true;
            soldier = npc.GetComponent<Soldier>();
        }


        if(Time.time > nextWaitTime)
        {
            waiting = false;
        }


        if (!soldier.walkingToWayPoint && !soldier.isCheckingForWaypoint && !waiting)
        {
            WalkToNextWaypoint();
        }

        if (!facingWP && !waiting)
        {
            //  Debug.Log("WALKTOWAYPOINT");
            WalkToNewWaypoint();
        }

        //Check distance last
        CheckDistanceToWaypoint();
    }

    public override void Reason(GameObject player, GameObject npc)
    {
        if (!firstFrame)
        {
            firstFrame = true;
            soldier = npc.GetComponent<Soldier>();
        }

        if (soldier.playerInSight)
        {
            soldier.SetTransition(Transition.GoToAttack);
        }

    }

    void CheckDistanceToWaypoint()
    {
        if (soldier.currentWaypoint == null)
            return;

       // Debug.Log(Vector3.Distance(new Vector3(soldier.transform.position.x, 0, soldier.transform.position.z), new Vector3(soldier.currentWaypoint.transform.position.x, 0, soldier.currentWaypoint.transform.position.z)));

        if (Vector3.Distance( new Vector3(soldier.transform.position.x, 0, soldier.transform.position.z), new Vector3(soldier.currentWaypoint.transform.position.x,0, soldier.currentWaypoint.transform.position.z)) < 1)
        {            
            if (!waiting && soldier.currentWaypoint.GetComponent<Waypoint>().waitTime > 0 && !soldier.isCheckingForWaypoint)
            {
                waiting = true;
                nextWaitTime = Time.time + soldier.currentWaypoint.GetComponent<Waypoint>().waitTime;
          //      Debug.Log("WAIT");
            }
            else
            {
                soldier.atWaypoint = true;
                soldier.walkingToWayPoint = false;
        //        Debug.Log("NOWAIT AND GO");
            }
        }
        else
        {
            soldier.atWaypoint = false;
        }

    }


    void WalkToNextWaypoint()
    {
        soldier.isCheckingForWaypoint = true;
      //  Debug.Log("IS CHECKING FOR WAYPOINT");

        for (int i = -1; i <= soldier.patrolWaypoints.Length; i++)
        {
            if (i == currentPatrolPoint)
            {
                if (soldier.currentWaypoint != null)
                {
                    soldier.currentWaypoint.GetComponent<Waypoint>().taken = false;
                }

                if(currentPatrolPoint < soldier.patrolWaypoints.Length -1)
                {
                    currentPatrolPoint++;
                    soldier.currentWaypoint = soldier.patrolWaypoints[currentPatrolPoint];
                }
                else
                {
                    currentPatrolPoint = 0;
                    soldier.currentWaypoint = soldier.patrolWaypoints[0];
                }

                soldier.currentWaypoint.GetComponent<Waypoint>().taken = true;
                soldier.agent.SetDestination(soldier.currentWaypoint.transform.position);
                soldier.isCheckingForWaypoint = false;
                facingWP = false;
                //Debug.Log(soldier.currentWaypoint);
                // Debug.Log("FOUNDWAYPOINT");
                return;
            }
        }               
                
    }


    void WalkToNewWaypoint()
    {
        if (soldier.currentWaypoint.GetComponent<Waypoint>().stopAndWait)
        {
            //rotate to destination
            Quaternion targetRotation = Quaternion.LookRotation(soldier.currentWaypoint.transform.position - soldier.agent.transform.position);
            soldier.agent.transform.rotation = Quaternion.RotateTowards(soldier.agent.transform.rotation, targetRotation, soldier.rotationSpeed *10 * Time.deltaTime);

            //if not facing destination speed = 0
            if (Quaternion.Angle(soldier.agent.transform.rotation, targetRotation) > 6)
            {
                soldier.walkingToWayPoint = true;
                soldier.agent.speed = 0;
            }        // else speed = speed
            else
            {

                facingWP = true;
                soldier.agent.speed = soldier.speed;
            }
        }
        else
        {
            Quaternion targetRotation = Quaternion.LookRotation(soldier.currentWaypoint.transform.position - soldier.agent.transform.position);
            soldier.agent.transform.rotation = Quaternion.RotateTowards(soldier.agent.transform.rotation, targetRotation, soldier.rotationSpeed*10 * Time.deltaTime);

            //rotate to destination
            soldier.walkingToWayPoint = true;
            facingWP = true;
            //soldier.agent.speed = soldier.speed;
        }


    }

}
