﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoldierAttackState : FSMState
{
    Soldier soldier;
    bool firstFrame;

    public SoldierAttackState()
    {
        stateID = StateID.SoldierAttackState;
    }


    public override void Act(GameObject player, GameObject npc)
    {
        if (!firstFrame || soldier == null)
        {
            soldier = npc.GetComponent<Soldier>();
            firstFrame = true;
        }




        if (soldier.agressor)
        {

            if (Vector3.Distance(soldier.agent.transform.position, Player.instance.transform.position) > 10)
            {
                soldier.agent.SetDestination(Player.instance.transform.position);
            }
            else
            {
                //if not at position
                if(soldier.agent.transform.position == soldier.agent.destination)
                {
                    Vector3 xz = UnityEngine.Random.insideUnitCircle * 8;
                    Vector3 newPosition = new Vector3(xz.x, 0, xz.y) + Player.instance.transform.position;
                    Debug.Log(newPosition);
                    
                    soldier.agent.SetDestination(newPosition);
                    if(soldier.agent.pathStatus == UnityEngine.AI.NavMeshPathStatus.PathComplete)
                    {
                        Debug.Log("COMPLETEPATH");
                    }
                    else
                    {
                        Debug.Log("INCOMPLETEPATH");
                    }

                }
                //soldier.agent.destination = soldier.agent.transform.position;
            }

            if (soldier.playerInSight)
            {
                soldier.Shoot();
            }

        }
        RotateToPlayer();

    }


    public override void Reason(GameObject player, GameObject npc)
    {
        if (!firstFrame || soldier == null)
        {
            soldier = npc.GetComponent<Soldier>();
            firstFrame = true;
        }
    }

    public void RotateToPlayer()
    {
        //soldier.transform.parent.rotation = Quaternion.RotateTowards(soldier.transform.parent.rotation,Player.instance.transform.rotation, -soldier.rotationSpeed * Time.deltaTime);
        // soldier.transform.parent.LookAt(Player.instance.transform.position);
        soldier.agent.updateRotation = false;

        Vector3 dir = Player.instance.transform.position - soldier.transform.parent.position;
        dir.y = 0;
        Quaternion rot = Quaternion.LookRotation(dir);
        soldier.transform.parent.rotation = Quaternion.Slerp(soldier.transform.parent.rotation, rot, soldier.rotationSpeed * Time.deltaTime);
    }
}
