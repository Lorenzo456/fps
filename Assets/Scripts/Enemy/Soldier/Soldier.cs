﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Soldier : BaseEnemy, IDamageable
{
    [Header("Waypoint Settings")]
    public GameObject[] patrolWaypoints;
    public bool walkingToWayPoint;
    public bool atWaypoint;
    public bool isCheckingForWaypoint;
    public bool playerInSight;
    public bool agressor;
    public GameObject currentWaypoint;

    public float rotationSpeed;

    public override void Start()
    {
        base.Start();
        weapon = GetComponentInChildren<Weapon>();
    }

    public override void MakeFSM()
    {
        SoldierIdleState idle = new SoldierIdleState();
        idle.AddTransition(Transition.GoToAttack, StateID.SoldierAttackState);

        SoldierAttackState attack = new SoldierAttackState();
        
        fsm = new FSMSystem();
        fsm.AddState(idle);
        fsm.AddState(attack);
        
    }

    public void OnHit(int damageTaken)
    {
        //Do something fun
        currentHp -= damageTaken;
        if(currentHp <= 0)
        {
            OnDeath();
        }
    }

    public int CheckHealth()
    {
        return currentHp;
    }

    public void Shoot()
    {
        if (weapon == null || weapon.reloading)
            return;

        if(Time.time > weapon.nextFireTime && weapon.currentClip > 0)
        {
            weapon.Shoot();
        }else if(weapon.currentClip <= 0 && !weapon.reloading)
        {
            weapon.Reload();
        }

    }
}
