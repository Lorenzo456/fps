﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour {

    public float bulletSpeed = 2;
    public int damage = 1;
    private WaitForSeconds lifeTime = new WaitForSeconds(10f);
    AudioSource audioSourceMetal;
    BoxCollider myCollider;
    Rigidbody rb;
    Renderer myRenderer;

    // Use this for initialization
    void Start () {
        StartCoroutine("CheckLifeTime");
        audioSourceMetal = GetComponent<AudioSource>();
        myCollider = GetComponent<BoxCollider>();
        rb = GetComponent<Rigidbody>();
        myRenderer = GetComponent<MeshRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
        //transform.position += transform.forward * bulletSpeed * Time.fixedDeltaTime;
        rb.velocity = (transform.forward * bulletSpeed *100) * Time.deltaTime;

    }

    IEnumerator CheckLifeTime()
    {
        yield return lifeTime;
        Destroy(gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Interactable"))
        {
            audioSourceMetal.Play();
            Player.instance.OnFeedBackHit();
            DisableBullet();
        }
        else
        {
            DisableBullet();
        }
    }

    void DisableBullet()
    {
        myCollider.enabled = false;
        rb.isKinematic = true;
        myRenderer.enabled = false;
        Destroy(gameObject, 3f);
    }
}
