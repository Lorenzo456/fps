﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileShootScript : MonoBehaviour {

    public Rigidbody projectile;
    public Transform muzzle;
    public float projectileForce = 500;
    public float fireRate = .25f;
    float bumperR;

    private float nextFireTime;

	// Use this for initialization
	void Start () {
        muzzle = GetComponentInChildren<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
        bumperR = Input.GetAxis("Fire2");
        Debug.Log(bumperR);
        if (bumperR > .5f && Time.time > nextFireTime)
        {
            Rigidbody cloneRb = Instantiate(projectile, muzzle.position, Quaternion.identity) as Rigidbody;
            cloneRb.AddForce(muzzle.transform.forward * projectileForce);
            nextFireTime = Time.time + fireRate;
        }
	}
}
