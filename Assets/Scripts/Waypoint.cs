﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waypoint : MonoBehaviour {

    public bool taken;
    public bool stopAndWait;
    public float waitTime;
}
